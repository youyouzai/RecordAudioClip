﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public Transform mRoadPanelTrans;
    public Transform mActorPanelTrans;

    public static GameManager Instance;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InitRoad();
        InitActor();
    }

    void InitRoad()
    {
        GameObject mRoadCreater = Instantiate(Resources.Load("RoadCreater")) as GameObject;
        Transform trans = mRoadCreater.transform;
        trans.SetParent(mRoadPanelTrans);
        trans.localScale = Vector3.one;
        trans.localPosition = Vector3.zero;
        RoadCreater.Instance.InitRoad();
    }

    void InitActor()
    {
        GameObject mActorController = Instantiate(Resources.Load("ActorController")) as GameObject;
        Transform trans = mActorController.transform;
        trans.SetParent(mActorPanelTrans);
        trans.localScale = Vector3.one;
        trans.localPosition = Vector3.zero;
        RoadCreater.Instance.AddListenerActorMove();
    }

    public void Restart()
    {       
        StartCoroutine(RestartIEnum());
    }

    IEnumerator RestartIEnum()
    {
        if (ActorController.Instance != null)
            Destroy(ActorController.Instance.gameObject);
        if (RoadCreater.Instance != null)
            Destroy(RoadCreater.Instance.gameObject);
        MainManager.Instance.SetScore(0);
        yield return new WaitForEndOfFrame();
        InitRoad();
        InitActor();
    }
}
