﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyMicphone : MonoBehaviour
{
    public static MyMicphone Instance;

    void Awake()
    {
        Instance = this;
    }

    private AudioSource mMyAudioSource;
    private AudioSource MyAudioSource
    {
        get
        {
            if (mMyAudioSource == null)
            {
                mMyAudioSource = gameObject.AddComponent<AudioSource>();
            }
            return mMyAudioSource;
        }
    }

    public Action<float> mSourceCallBack;

    private int deviceCount;
    private string sFrequency = "8000";

    public string sLog = "";

    void Start()
    {
        string[] ms = Microphone.devices;
        deviceCount = ms.Length;
        if (deviceCount == 0)
        {
            Log("no microphone found");
        }
        else if (deviceCount > 0)
        {
            StartRecord();
            StartCoroutine("SetAudioValue");            
        }
    }

    IEnumerator SetAudioValue()
    {
        int micPos = 0; 
        while (true)
        {
            if (Microphone.IsRecording(null))
            {
                int startPos = micPos;
                int endPos = Microphone.GetPosition(null);
                if (endPos < MyAudioSource.clip.samples)
                {
                    if (endPos > startPos)
                    {
                        //float[] samples = new float[MyAudioSource.clip.samples];
                        //MyAudioSource.clip.GetData(samples, 0);

                        float[] samples = new float[endPos - startPos];
                        MyAudioSource.clip.GetData(samples, startPos);

                        float audioValue = 0;
                        for (int i = 0; i < samples.Length; i++)
                        {
                            if (samples[i] > audioValue)
                            {
                                audioValue = samples[i];
                            }
                        }

                        if (mSourceCallBack != null)
                        {
                            mSourceCallBack(audioValue);
                        }
                    }
                    micPos = endPos;
                }
            }
            else
            {
                StartRecord();
                micPos = 0;
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    
    void Log(string log)
    {
        //sLog += log;
        //sLog += "\r\n";
    }

    void OnGUI()
    {
        GUILayout.Label(sLog);
    }

    void StartRecord()
    {
        MyAudioSource.Stop();
        MyAudioSource.loop = false;
        MyAudioSource.mute = true;
        MyAudioSource.clip = Microphone.Start(null, false, 1000, int.Parse(sFrequency));
        while (!(Microphone.GetPosition(null) > 0))
        {
        }
        MyAudioSource.Play();
        Log("StartRecord");
    }

    void StopRecord()
    {
        if (!Microphone.IsRecording(null))
        {
            return;
        }
        Microphone.End(null);
        MyAudioSource.Stop();
        Log("StopRecord");
    }

    void PlayRecord()
    {
        if (Microphone.IsRecording(null))
        {
            return;
        }
        if (MyAudioSource.clip == null)
        {
            return;
        }
        MyAudioSource.mute = false;
        MyAudioSource.loop = false;
        MyAudioSource.Play();
    }
}