﻿using UnityEngine;
using System.Collections;

public class MainManager : MonoBehaviour
{
    public GameObject mReStartBtn;
    public UILabel mScoreLab;

    public static MainManager Instance;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        mScoreLab.text = "0";
        UIEventListener.Get(mReStartBtn).onClick = go => { GameManager.Instance.Restart(); };
    }

    public void SetScore(float tValue)
    {
        mScoreLab.text = ((int)tValue).ToString();
    }

    void OnDestroy()
    {

    }
}
