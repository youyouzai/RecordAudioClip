﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadCreater : MonoBehaviour
{
    private List<Transform> mRoadList = new List<Transform>();
    private Object mRoadAsset;
    private Object mMonsterAsset;

    private int curRoadId;

    public static RoadCreater Instance;
    void Awake()
    {
        Instance = this;
    }

    public void InitRoad()
    {
        curRoadId = 0;

        mRoadAsset = Resources.Load("RoadItem");
        Vector3 pos = new Vector3(-Config.mScreenPixelSize.x / 2, -Config.mScreenPixelSize.y / 2, 0);
        Vector2 size = new Vector2(Config.mScreenPixelSize.x * 1.5f, Config.mScreenPixelSize.y / 1.5f);
        CreateRoad(pos, size);
        while (true)
        {
            Transform lastTrans = mRoadList[mRoadList.Count - 1];
            UISprite sprite = lastTrans.GetComponent<UISprite>();
            float boundx = lastTrans.localPosition.x + sprite.width / 2;
            if (boundx < Config.mScreenPixelSize.x)
                CreateRoad();
            else
                break;
        }
    }

    void CreateRoad(Vector3 tPos, Vector2 tSize)
    {
        GameObject roadItem = Instantiate(mRoadAsset) as GameObject;
        Transform roadTrans = roadItem.transform;
        roadTrans.SetParent(transform);
        roadTrans.localPosition = tPos;
        roadTrans.localScale = Vector3.one;
        UISprite sprite = roadItem.GetComponent<UISprite>();
        sprite.width = (int)tSize.x;
        sprite.height = (int)tSize.y;
        mRoadList.Add(roadTrans);

        IncreaseRoadCount();
    }

    void CreateRoad()
    {
        GameObject roadItem = Instantiate(mRoadAsset) as GameObject;
        Transform roadTrans = roadItem.transform;
        roadTrans.SetParent(transform);
        roadTrans.localScale = Vector3.one;
        UISprite sprite = roadItem.GetComponent<UISprite>();
        sprite.width = Random.Range(200, 1000);
        sprite.height = Random.Range(400, 1000);
        Transform lastTrans = mRoadList[mRoadList.Count - 1];
        UISprite lastSprite = lastTrans.GetComponent<UISprite>();
        int boundx = (int)lastTrans.localPosition.x + lastSprite.width / 2;

        int x = boundx + sprite.width / 2 + Random.Range(150, 300);
        int y = -(int)Config.mScreenPixelSize.y / 2;
        int z = 0;
        roadTrans.localPosition = new Vector3(x, y, z);
        mRoadList.Add(roadTrans);

        IncreaseRoadCount();
    }

    void IncreaseRoadCount()
    {
        curRoadId++;
        if (curRoadId > 10)
            TryCreateMonster();
    }

    /// <summary>
    /// 创建怪物
    /// </summary>
    void TryCreateMonster()
    {
        float random = Random.Range(0.0f, 1.0f);
        if (random < 0.5f)
            return;
        
        if (mMonsterAsset == null)
        {
            mMonsterAsset = Resources.Load("Monster");
        }
        Transform lastTrans = mRoadList[mRoadList.Count - 1];
        UISprite roadSprite = lastTrans.GetComponent<UISprite>();
        GameObject monster = Instantiate(mMonsterAsset) as GameObject;
        UISprite monsterSprite = monster.GetComponent<UISprite>();

        if (roadSprite.width / monsterSprite.width < 5)
        {
            Destroy(monster);
            return;
        }

        Transform monsterTrans = monster.transform;
        monster.transform.SetParent(lastTrans);
        monsterTrans.localScale = Vector3.one;

        float startX = (-roadSprite.width + monsterSprite.width) / 2;
        float endX = (roadSprite.width - monsterSprite.width) / 2;
        float y = (roadSprite.height + monsterSprite.height) / 2;
        Vector3 mStartPos = new Vector3(startX, y, 0);
        Vector3 mEndPos = new Vector3(endX, y, 0);
        monsterTrans.localPosition = (mStartPos + mEndPos) / 2;
        MonsterController monsterScript = monsterTrans.GetComponent<MonsterController>();
        monsterScript.Init(mStartPos, mEndPos);
    }

    void ListenerMoveCallBack()
    {
        if (CheckBoundOver())
        {
            CreateRoad();
            TryDeleteRoad();
        }
    }

    bool CheckBoundOver()
    {
        Transform lastTrans = mRoadList[mRoadList.Count - 1];
        UISprite sprite = lastTrans.GetComponent<UISprite>();
        float boundx = lastTrans.localPosition.x + sprite.width / 2;
        float actorx = ActorController.Instance.transform.localPosition.x;
        if (boundx - actorx < Config.mScreenPixelSize.x / 2)
            return true;
        else
            return false;
    }

    void TryDeleteRoad()
    {
        Transform firstTrans = mRoadList[0];
        UISprite lastSprite = firstTrans.GetComponent<UISprite>();
        int boundx = (int)firstTrans.localPosition.x + lastSprite.width / 2;
        float actorx = ActorController.Instance.transform.localPosition.x;
        if (actorx - boundx > Config.mScreenPixelSize.x / 2)
        {
            mRoadList.RemoveAt(0);
            Destroy(firstTrans.gameObject);
        }
    }

    /// <summary>
    /// 添加角色移动监听
    /// </summary>
    public void AddListenerActorMove()
    {
        if (ActorController.Instance != null)
            ActorController.Instance.mMoveCallBack = ListenerMoveCallBack;
    }
    /// <summary>
    /// 移除角色移动监听
    /// </summary>
    public void MoveListenerActorMove()
    {
        if (ActorController.Instance != null)
            ActorController.Instance.mMoveCallBack = null;
    }

    void OnDestroy()
    {
        Debug.Log("Road_OnDestroy");
        MoveListenerActorMove();
        Instance = null;
    }
}
