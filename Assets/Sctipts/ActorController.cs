﻿using UnityEngine;
using System.Collections;
using System;

public class ActorController : MonoBehaviour
{
    public Transform mModelTrans;
    public Transform mModelFootTrans;
    private Rigidbody2D mModelRigidbody2D;

    private bool mMove = false;
    private Vector3 mMoveMinSpeed = Vector3.zero;
    private Vector3 mMoveMaxSpeed = Vector3.right * 10;
    private Vector3 mMoveSpeed = Vector3.zero;

    public Action mMoveCallBack;

    private float mStartX = 0;

    public static ActorController Instance;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        mModelRigidbody2D = mModelTrans.GetComponent<Rigidbody2D>();
        AddListenerSource();

        mStartX = transform.localPosition.x;

        StartCoroutine("UpdateEvent");
    }

    void CallBack(float audioValue)
    {
        if (audioValue > 0.05f)
        {
            mMove = true;
            if (audioValue > 0.2f && audioValue < 1.0f)
            {
                if (Physics2D.Linecast(mModelTrans.position, mModelFootTrans.position, 1 << LayerMask.NameToLayer("Road")))
                {
                    mModelRigidbody2D.AddForce(Vector2.up * 150f * audioValue);
                    MyMicphone.Instance.sLog = "跳跃";
                }
            }
        }
        else
        {
            mMove = false;
        }
    }

    IEnumerator UpdateEvent()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (mMove)
            {
                Vector3 vec3 = transform.localPosition;
                vec3 += mMoveMaxSpeed;
                transform.localPosition = vec3;

                if (mMoveCallBack != null)
                {
                    mMoveCallBack();
                }
                MainManager.Instance.SetScore(transform.localPosition.x - mStartX);
            }
            else
            {
                if (mMoveSpeed.x > mMoveMinSpeed.x)
                {
                    mMoveSpeed -= mMoveMaxSpeed * Time.deltaTime;
                }
                if (mMoveSpeed.x < mMoveMinSpeed.x)
                {
                    mMoveSpeed = mMoveMinSpeed;
                }
                if (mMoveSpeed.x != mMoveMinSpeed.x)
                {
                    Vector3 vec3 = transform.localPosition;
                    vec3 += mMoveSpeed;
                    transform.localPosition = vec3;

                    if (mMoveCallBack != null)
                    {
                        mMoveCallBack();
                    }
                    MainManager.Instance.SetScore(transform.localPosition.x - mStartX);
                }
            }
            CheckDie();
        }
    }

    void CheckDie()
    {
        float y = mModelTrans.localPosition.y + Config.mScreenPixelSize.y / 2;
        if (y < -200)
        {
            Die();
        }
    }

    public void Die()
    {
        mModelRigidbody2D.isKinematic = true;
        MyMicphone.Instance.mSourceCallBack = null;
        StopCoroutine("UpdateEvent");
    }

    /// <summary>
    /// 添加声音监听
    /// </summary>
    public void AddListenerSource()
    {
        if (MyMicphone.Instance != null)
            MyMicphone.Instance.mSourceCallBack = CallBack;
    }
    /// <summary>
    /// 移除声音监听
    /// </summary>
    public void MoveListenerSource()
    {
        if (MyMicphone.Instance != null)
            MyMicphone.Instance.mSourceCallBack = null;
    }

    void OnDestroy()
    {
        Debug.Log("Actor_OnDestroy");
        MoveListenerSource();
        Instance = null;
    }
}
