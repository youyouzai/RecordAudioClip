﻿using UnityEngine;
using System.Collections;
using System;

public class MonsterController : MonoBehaviour
{
    private Vector3 mStartPos;
    private Vector3 mEndPos;
    private Vector3 mSpeed;
    private enum MoveFlag
    {
        right,
        left,
    }
    private MoveFlag mMoveFlag;

    public void Init(Vector3 tStartPos, Vector3 tEndPos)
    {
        mStartPos = tStartPos;
        mEndPos = tEndPos;
        mMoveFlag = MoveFlag.right;
    }

    void Update()
    {
        if (transform.localPosition.x < mStartPos.x )
        {
            mMoveFlag = MoveFlag.right;
            Vector3 vec = transform.localScale;
            vec.x = -1;
            transform.localScale = vec;
        }
        else if (transform.localPosition.x > mEndPos.x)
        {
            mMoveFlag = MoveFlag.left;
            Vector3 vec = transform.localScale;
            vec.x = 1;
            transform.localScale = vec;
        }
        Move();
    }

    void Move()
    {
        Vector3 vec = transform.localPosition;
        switch (mMoveFlag)
        {
            case MoveFlag.left:
                vec += Vector3.left * 2;
                break;
            case MoveFlag.right:
                vec += Vector3.right * 2;
                break;
        }
        transform.localPosition = vec;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == ActorController.Instance.gameObject)
        {
            ActorController.Instance.Die();
        }
    }
}
