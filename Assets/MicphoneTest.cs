﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MicphoneTest : MonoBehaviour
{
    AudioSource _audio;
    AudioSource audio
    {
        get
        {
            if (_audio == null)
            {
                _audio = gameObject.AddComponent<AudioSource>();
            }
            return _audio;
        }
    }

    public Transform trans;

    void Start()
    {
        string[] ms = Microphone.devices;
        deviceCount = ms.Length;
        if (deviceCount == 0)
        {
            Log("no microphone found");
        }
        StartCoroutine("SetAudioValue");
    }

    IEnumerator SetAudioValue()
    {
        int micPos = 0; 
        while (true)
        {
            if (Microphone.IsRecording(null))
            {
                int startPos = micPos;
                int endPos = Microphone.GetPosition(null);
                if (endPos < audio.clip.samples)
                {
                    Log("startPos = " + startPos + "  endPos = " + endPos);
                    if (endPos > startPos)
                    {
                        float[] samples = new float[audio.clip.samples];
                        audio.clip.GetData(samples, 0);
                        float audioValue = 0;
                        for (int i = startPos; i < endPos; i++)
                        {
                            if (samples[i] > audioValue)
                            {
                                audioValue = samples[i];
                            }
                        }
                        Log("声音大小: " + audioValue);
                        trans.localPosition = Vector3.up * audioValue;
                    }
                    micPos = endPos;
                }
                else
                {
                    Log("startPos = " + startPos + "  endPos = " + endPos);
                    Log("停止录制");
                    StopRecord();
                    micPos = 0;
                }
            }
            else
            {
                micPos = 0;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    string sLog = "";
    void Log(string log)
    {
        sLog += log;
        sLog += "\r\n";
    }
    int deviceCount;
    string sFrequency = "10000";
    void OnGUI()
    {
        if (deviceCount > 0)
        {
            GUILayout.BeginHorizontal();
            if (!Microphone.IsRecording(null) && GUILayout.Button("Start", GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5)))
            {
                StartRecord();
            }
            if (Microphone.IsRecording(null) && GUILayout.Button("Stop", GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5)))
            {
                StopRecord();
            }
            if (!Microphone.IsRecording(null) && GUILayout.Button("Play", GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5)))
            {
                PlayRecord();
            }
            if (!Microphone.IsRecording(null) && GUILayout.Button("Print", GUILayout.Height(Screen.height / 20), GUILayout.Width(Screen.width / 5)))
            {
                PrintRecord();
            }
            sFrequency = GUILayout.TextField(sFrequency, GUILayout.Width(Screen.width / 5), GUILayout.Height(Screen.height / 20));
            GUILayout.EndHorizontal();
        }
        GUILayout.Label(sLog);
    }
    void StartRecord()
    {
        audio.Stop();
        audio.loop = false;
        audio.mute = true;
        audio.clip = Microphone.Start(null, false, 1000, int.Parse(sFrequency));
        while (!(Microphone.GetPosition(null) > 0))
        {
        }
        audio.Play();
        Log("StartRecord");
    }

    void StopRecord()
    {
        if (!Microphone.IsRecording(null))
        {
            return;
        }
        Microphone.End(null);
        audio.Stop();
    }
    void PrintRecord()
    {
        if (Microphone.IsRecording(null))
        {
            return;
        }
        byte[] data = GetClipData();
        string slog = "total length:" + data.Length + " time:" + audio.time;
        Log(slog);
    }
    void PlayRecord()
    {
        if (Microphone.IsRecording(null))
        {
            return;
        }
        if (audio.clip == null)
        {
            return;
        }
        audio.mute = false;
        audio.loop = false;
        audio.Play();
    }
    public byte[] GetClipData()
    {
        if (audio.clip == null)
        {
            Debug.Log("GetClipData audio.clip is null");
            return null;
        }

        float[] samples = new float[audio.clip.samples];

        audio.clip.GetData(samples, 0);


        byte[] outData = new byte[samples.Length * 2];

        int rescaleFactor = 32767;

        for (int i = 0; i < samples.Length; i++)
        {
            short temshort = (short)(samples[i] * rescaleFactor);

            byte[] temdata = System.BitConverter.GetBytes(temshort);

            outData[i * 2] = temdata[0];
            outData[i * 2 + 1] = temdata[1];


        }
        if (outData == null || outData.Length <= 0)
        {
            Debug.Log("GetClipData intData is null");
            return null;
        }
        return outData;
    }

}